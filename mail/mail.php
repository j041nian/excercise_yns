<?PHP

#PHPMailer Library for mail

require_once('PHPMailer/PHPMailer.php');
require_once('PHPMailer/Exception.php');
require_once('PHPMailer/SMTP.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

$address = $_GET['mailto'];
$subject = $_GET['subject'];
$msg     = $_GET['msg'];
$error = 0;
$arr_error = array();

// Validation rule
if (strlen($address) > 50 ) {
    $error = 1;
    $arr_error = [
        'email_length' => 'Email only accepts 50 characters!'
    ];
}

if (!filter_var($address,FILTER_VALIDATE_EMAIL)){
    $error = 1;
    $arr_error = [
        'email_format' => "Invalid Email format!"
    ];
}

if (strlen($subject) > 50) {
    $error = 1;
    $arr_error = [
        'subject' => 'Subject only accepts 50 characters!'
    ];
}


$mail = new PHPMailer;

    //Server settings
    $mail->SMTPDebug = 0;
    $mail->isSMTP();
    $mail->Host = 'smtp.live.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'jodan_ian@hotmail.com';
    $mail->Password = base64_decode("ajA0MW5pYW5u");
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->From = 'jodan_ian@hotmail.com';
    $mail->FromName = 'PHPMailer';
    $mail->addAddress($address);

    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body    = $msg;

    if(!$mail->send() || $error == 1) {

        if (!empty($arr_error)) {
            echo "<h1>Error</h1>";
            foreach ($arr_error as $str_err) {
                echo $str_err."<br>";
            }
        }
    ?>
        <script>
            alert("Message Not Sent");
        </script>
    <?PHP

    if ($error == 0) {
        $mail->ErrorInfo;
    }

} else {
        header('Location: index.php?success');
}
?>
