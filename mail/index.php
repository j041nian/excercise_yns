<!DOTYPE html>
<html>

<head>
    <title>Web Application</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<style>
    html body {
        background: #f0f0f0;
    }

    textarea {
        height: 300px;
    }

</style>

<body>

    <?PHP
        if(isset($_GET['success'])) {
            echo "<script>alert('Message has been sent!')</script>";
        }
    ?>

    <div class="container">

        <h3>Contact Form:</h3>

        <form method="get" action="mail.php">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-1">
                            <label for="mailto">Mail To:</label>
                        </div>
                        <div class="col-md-6">
                            <input type="email" class="form-control" id="mailto" name="mailto" placeholder="(example@yahoo.com)" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-1">
                            <label for="subject">Subject:</label>
                        </div>
                        <div class="col-md-6">
                            <input id="subject" type="text" class="form-control" name="subject" required>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-1">
                            <label for="msg">Message:</label></div>
                        <div class="col-md-6">
                            <textarea class="form-control" name="msg" required></textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-7">
                            <button class="btn btn-outline-primary float-right">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>

</html>
