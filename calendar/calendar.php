<?PHP

namespace Calendar;

class Calendar{

    private static $weekDay = [
      0 =>  'Sun',
      1 => 'Mon',
      2 =>  'Tue',
       3 => 'Wed',
       4 => 'Thu',
       5 => 'Fri',
       6 => 'Sat'
    ];

    private static $fday;
    private static $lday;
    private static $fday_D;
    private static $year;
    private static $month;
    private static $month_M;

    public function build($strdate = NULL){
        if (is_null($strdate)){
            $year = date("Y");
            $month_M = date("M");
            $month = date('m');
        } else {
            $year = date("Y",strtotime($strdate));
            $month = date('m',strtotime($strdate));
            $month_M = date("M",strtotime($strdate));
        }

        self::$year = $year;
        self::$month = $month;
        self::$month_M = $month_M;

        $days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
        self::$fday_D = date("D",strtotime("first day of $month_M $year"));
        self::$fday = date("d",strtotime("first day of $month_M $year"));
        self::$lday = date("d",strtotime("last day of $month_M $year"));

        $next = date("M Y",strtotime("$month_M $year + 1month"));
        $last = date("M Y",strtotime("$month_M $year - 1month"));

        echo "<table class='table table-striped table-responsive'>";
        echo "<thead class='table-success'>";
            echo "<td><a href='?date=".$last."' class='btn btn-primary'><span class='fa fa-angle-left'></span></a></td>";
            echo "<td colspan='5' class='text-center'><b>".strtoupper(date("F",strtotime("$month_M $year")))."</b></td>";
            echo "<td><a href='?date=".$next."' class='btn btn-primary'><span class='fa fa-angle-right'></span></a></td>";
        echo "</thead>";
        echo "<tbody>";
                foreach(self::$weekDay as $index => $day){
                    echo "<th>".$day."</th>";
                }
                $this->weeks();
        echo "</tbody>";
        echo "<tfoot>";
        echo "<td colspan='7' class='text-center'><b>".$year."</b></td>";
        echo "</tfoot>";
        echo "</table>";
    }

    private function weeks(){
        $num_week = 0;
        $count_week = 0;
        $day = 0;
        $ref = array_flip(self::$weekDay);
        while($num_week < self::$lday) {
            $num_week += 7;
            $count_week++;
        }

        $ref_date = date("d",strtotime("last saturday of ".self::$month_M." ".self::$year));

        if ( ($ref_date+7) >= self::$lday  ) {
            $count_week++;
        }

        for ($i=1;$i<= $count_week;$i++) {
            echo "<tr>";

            for ($j=0;$j<=6;$j++) {

                if ($i == 1 && $j == $ref[self::$fday_D] && $day == 0) {
                  echo "<td class='day'>".++$day."</td>";
                } elseif ( $day <> 0) {
                    if ($day > (self::$lday - 1)) {
                        echo "<td></td>";
                    } else {
                       if (date("Y-m-d",strtotime("today - 1day")) == date("Y-m-d",strtotime(self::$year."-".self::$month."-".$day)) ) {
                            echo "<td class='day'><i class='circle'>".++$day."</i></td>";
                       } else {
                            echo "<td class='day'>".++$day."</td>";
                       }
                    }
                } else {
                    echo "<td></td>";
                }
            }
            echo "</tr>";
        }
    }
}
?>
