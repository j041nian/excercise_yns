<!DOCTYPE html>
<html>

<head>
    <title>Calendar</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="js/calendar.js"></script>
</head>

<style>
    html body {
        background: #f0f0f0;
    }

    table th {
        padding: 20px !important;
    }

    .day:hover {
       background: #000;
       color: #FFF;
    }

    .content {
        width: 500px;
        margin: 0 auto;
        text-align: center;
        padding-top: 50px;
        cursor: pointer;
    }

    .circle {
        height: 100%;
        width: 100%;
        background: red;
        padding: 5px;
        font-style: normal;
    }
</style>

<body>
    <div class="container">
        <?PHP
            require('calendar.php');
            use Calendar\Calendar;
            $calendar = new Calendar;
        ?>
        <div class="content">
           <?PHP //generate calendar
            $date = NULL;
            if(isset($_GET['date'])) {
                $date = $_GET['date'];
            }
            $calendar->build($date);
            ?>
        </div>
    </div>
</body>

</html>
