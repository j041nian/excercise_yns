<!DOCTYPE html>
<html>
<head>
    <title>Upload images</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
    <body>
        <!--
            Add upload image feature for uploading user profile image.
        -->

    <style>
        .form-input {
            width: 500px;
            margin: 0 auto;
            padding-top: 70px;
        }
        margin: 0 auto;
    </style>
<?PHP
    require('session.php');
    require('dbconfig.php');
    $session = new Session;
    $Db = new Db;
?>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-fixed-top">
    <a class="navbar-brand" href="logout.php"><span class="fa fa-logout"></span>Logout <?=$session->is_logged();?></a>
  </nav>

<div class="container">


    <div class="col-md-12">
        <br>
                   <a href="list.php" class="btn btn-light"><span class="fa fa-list"></span> Information List</a>
        <div class="form-input">
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
            <kabel>Upload Image:</kabel>
             <input class="form-control" type='file' name='picture'>
            </div>

             <input class="btn btn-success" type="submit" name="submit" value="Submit">
        </form>
    </div>
    </div>

</div>
        <?PHP
            if(isset($_POST['submit'])){
            $base_dir = 'images/';
            $file = $_FILES['picture']['name'];
            $name = "picture".$_GET['id'].".".explode(".",$file)[1];
                    if(move_uploaded_file($_FILES['picture']['tmp_name'],$base_dir.$name)){
                        $sql = "INSERT INTO tbl_files(filename,info_id)
                                VALUES(?,?)";
                        $result =$Db->query($sql,[$name,$_GET['id']]);
                        if($result){
                            $session->alert("File saved to Database!");
                        } else {
                            $session->alert("ERROR: File was not uploaded!");
                        }
                    } else {
                        $session->alert("ERROR: File was not uploaded!");
                    }
            }

            if(empty($_GET['id'])) {
                header("Location: list.php");
            }
        ?>
    </body>
</html>
