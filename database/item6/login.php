<!DOCTYPE html>
<html>
<head>
    <title>Web Application</title>
       <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

<style>
    .login {
        width: 500px;
        margin: 0 auto;
        padding-top: 100px;
    }

    h5 {
        text-align: center;
    }
</style>
<div class="container">

<div class="login">
        <?PHP
        if(isset($_GET['error'])){
            echo '<div class="alert alert-danger alert-dismissable">';
            echo '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
            switch($_GET['error']) {
                case 1:
                    echo "<strong>ERROR:</strong> All Fields are Required!";
                    break;
                case 2:
                    echo "<strong>ERROR:</strong> Wrong Login Credentials";
                    break;
            }
            echo "</div>";
        }
    ?>

    <h5><span class="fa fa-lock"></span> Login Form</h5>
    <form method="post" action="credentials.php">
        <div class="col-md-12">
            <div class="form-group">
            <label>Username:</label>
            <input type="text" class="form-control" name="user" required autocomplete="off">
            </div>

            <div class="form-group">
            <label>Password:</label>
            <input type="password" class="form-control" name="pass" required>
            </div>

            <button type="submit" class="btn btn-outline-success">Login</button>
        </div>
    </form>
</div>
    <!-- /.login -->
</div>
    <!-- /.Container -->
</body>
</html>
