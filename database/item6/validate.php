<?PHP
class Validation {

        //Checks if field is empty / Null
        function checkRequired($x){
            if(empty($x) || $x == " " || is_null($x)){
                return "ERROR: Field is REQUIRED!";
            } else {
                return $x;
            }
        }

        // Checks if age is numeric
        function checkAge($x) {
            if(is_numeric($x)){
                return $this->checkRequired($x);
            } else {
                return "ERROR: age must be NUMERIC!";
            }
        }

        // Checks if email follows format
        function checkEmail($x){
            if(!filter_var($x,FILTER_VALIDATE_EMAIL)){
                if(empty($x)) {
                    return checkRequired($x);
                } else {
                    return "ERROR: Invalid EMAIL FORMAT!";
                }
            } else {
                return $x;
            }
        }

        // Checks if contains numbers
        function checkChar($x) {
            if(preg_match('#[0-9]#',$x)){
                return "ERROR: Name must NOT CONTAIN NUMBERS!";
            } else {
                return $this->checkRequired($x);
            }
        }
}

        require('session.php');
        require('dbconfig.php');
        $session = new Session;
        use App\Database;
        $validate = new Validation;
         if(isset($_POST['submit'])){
             $error = 0;
             $msg = array();

             $fname = $_POST['fname'];
             $lname = $_POST['lname'];
             $email = $_POST['email'];
             $age   = $_POST['age'];

             if($validate->checkChar($fname) !== $fname){
                 $msg['fname'] = $validate->checkChar($fname);
                 $error = 1;
             }

             if($validate->checkChar($lname) !== $lname){
                 $msg['lname'] = $validate->checkChar($lname);
                 $error = 1;
             }

             if($validate->checkEmail($email) !== $email){
                 $msg['email'] = $validate->checkEmail($email);
                 $error = 1;
             }

             if($validate->checkAge($age) != $age){
                 $msg['age'] = $validate->checkAge($age);
                 $error = 1;
             }

            // Displays Errors
             if($error == 1){
                 $session->error_msg($msg);
                 header('Location: home.php');
             } else {

                //Stores to Database
                $insert = $Db->query("INSERT INTO tbl_info(fname,lname,email,age)
                                      VALUES(?,?,?,?)", [
                                          $fname,
                                          $lname,
                                          $email,
                                          $age
                                      ]);

                 if($insert){
                     $session->success("$fname has been saved!");
                 }
                 header("Location: home.php");
             }




         }
?>
