<!DOCTYPE html>
<html>

<head>
    <title>Employee Information</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<style>
    label:before {
        color: red;
        content: '*';
        margin-right: 3px;
    }

    .form-input {
        width: 600px;
        margin: 0 auto;
    }

</style>

<body>
    <?PHP
    require_once('session.php');
    $session = new Session;
?>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-fixed-top">
            <a class="navbar-brand" href="logout.php"><span class="fa fa-logout"></span>Logout <?=$session->is_logged();?></a>
        </nav>

        <div class="container">

            <!--
        <pre>
        <?=var_dump($session->show_error());?>
        </pre>
-->
            <div class="col-md-12 ">
                <div class="row">
                    <div class="form-input">
                        <h2>Registration Form:</h2>
                        <form method="post" action="validate.php">
                            <div class="form-group">
                                <label>Enter First Name:</label>
                                <span class="text-danger font-italic">
                    <?=isset($session->show_error()['fname']) ? $session->show_error()['fname']:'';?>
                </span>
                                <input type="text" class="form-control" id="fname" name="fname" autocomplete="off" required>
                            </div>

                            <div class="form-group">
                                <label>Enter Last Name:</label>
                                <span class="text-danger font-italic">
                    <?=isset($session->show_error()['lname']) ? $session->show_error()['lname']:'';?>
                </span>
                                <input type="text" class="form-control" id="lname" name="lname" autocomplete="off" required>
                            </div>

                            <div class="form-group">
                                <label>Email Address:</label>
                                <span class="text-danger font-italic">
                    <?=isset($session->show_error()['email']) ? $session->show_error()['email']:'';?>
                </span>
                                <input type="text" class="form-control" id="email" name="email" autocomplete="off" required>
                            </div>

                            <div class="form-group">
                                <label>Age:</label>
                                <span class="text-danger font-italic">
                    <?=isset($session->show_error()['age']) ? $session->show_error()['age']:'';?>
                </span>
                                <input type="text" class="form-control" id="age" name="age" autocomplete="off" required>
                            </div>

                            <input type="submit" class="btn btn-success" name="submit" value="Submit">
                        </form>
                    </div>
                </div>
            </div>

            <p>&nbsp;</p>

            <div class="col-md-12">
                <div class="row">
                    <div class=" col-md-2">
                        <a href="list.php" class="btn btn-info"><span class="fa fa-list"></span> Information List</a>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>
