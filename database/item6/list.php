<!DOCTYPE html>
<html>

<head>
    <title>Information List</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>

<body>
    <!--
            Add upload image feature for uploading user profile image.
        -->

    <?PHP
        require('session.php');
        $session = new Session;
        ?>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-fixed-top">
            <a class="navbar-brand" href="logout.php"><span class="fa fa-logout"></span>Logout <?=$session->is_logged();?></a>
        </nav>

        <div class="container">


            <p>&nbsp;</p>
            <div class="col-md-12">
                <a href="home.php" class="btn btn-light"><span class="fa fa-home"></span> Home</a>
                <form method="post" enctype="multipart/form-data">
                    <table class="table table-bordered table-striped table-hover table-sm">
                        <thead class="table-info">
                            <th>#</th>
                            <th width='125'>Pic</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Age</th>
                        </thead>
                        <tbody>
                            <?PHP
                require("dbconfig.php");
                use App\Database;
                $Db = new Db;
                $limit = 10;
                $offset = 0;

                if(isset($_GET['pages'])) {
                    $offset = ($_GET['pages'] - 1) * 10;
                }

                $sql = "SELECT *,tbl_info.id FROM tbl_info
                        LEFT JOIN tbl_files ON tbl_files.info_id = tbl_info.id
                        ORDER BY tbl_info.id ASC
                        LIMIT $limit OFFSET $offset";

                $result = $Db->query($sql);
                $count = $Db->query("
                            SELECT count(*) as num FROM tbl_info
                          ");
                $i = 1;

                if(isset($_GET['pages']) && $_GET['pages'] <> 1) {
                    $i += ($_GET['pages'] -1 )*10;
                }

                foreach($result as $row){
                    echo "<tr>";
                    echo "<td>".$i++."</td>";
                    echo "<td>".profile($row['filename'],$row['id'])."</td>";
                    echo "<td>".$row['fname']."</td>";
                    echo "<td>".$row['lname']."</td>";
                    echo "<td>".$row['email']."</td>";
                    echo "<td>".$row['age']."</td>";
                    echo "</tr>";
                }

                function profile($file,$id) {
                    $base = "images/";

                    if(empty($file)) {
                        return "
                            <a href='upload.php?id=$id' class='btn btn-light btn-xs'><span class='fa fa-upload'></span> Upload Pic</a>
                        ";
                    } else {
                        return "<img src='$base$file' width='100'>";
                    }
                }


                function pagination($x){

                    $num = $x/10;
                    if($x%10 != 0) {
                        $num++;
                    }

                    $page = "<ul class='pagination'>";
                    for($i=1;$i<=$num;$i++) {
                        if(isset($_GET['pages']) && $_GET['pages'] == $i) {
                             $page .= "<li class='active page-item'><a class='page-link' href='#'>$i</a></li>";
                        } else {
                            if($i == 1 && !isset($_GET['pages'])) {
                                $page .= "<li class='active page-item'><a class='page-link' href='#'>$i</a></li>";
                            } else {
                                $page .= "<li class='page-item'><a class='page-link' href='?pages=$i'>$i</a></li>";
                            }
                        }
                    }
                    $page .= "</ul>";
                    echo $page;
                }
                ?>
                        </tbody>
                    </table>
                </form>
                <div class="col-md-12">
                    <?=pagination($count[0]['num']);?>
                </div>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.container -->
</body>

</html>
