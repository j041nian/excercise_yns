<?PHP
class Db {

    private $Db;
    private static $instance = NULL;

    public function __construct(){
        try {
             $this->Db = new \PDO("mysql:host=127.0.0.1;dbname=user_info","root","");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function query($sql,$param = array()) {
        try {
         $query = $this->Db->prepare($sql);
         $result = $query->execute($param);
        } catch (PDOExeption $e) {
            return $e->getMessage();
        }

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function login($u,$p) {
        return $this->query("SELECT * FROM tbl_user WHERE user = ? AND pass = ?",
                              [$u,md5($p)]);
    }
}
$Db = new Db;
?>
