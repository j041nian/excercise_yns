<?PHP
class Session{

    private $error = "";
    private $logged = "";
    private $instance = NULL;
    private $success = "";

    public function __construct(){
        session_start();
        if(isset($_SESSION))
        {
          if(isset($_SESSION['user'])){
              $this->logged = $_SESSION['user'];
          }

          if(isset($_SESSION['error'])) {
              $this->error = $_SESSION['error'];
              unset($_SESSION['error']);
          }

          if(isset($_SESSION['success'])) {
              $this->success = $_SESSION['success'];
              unset($_SESSION['success']);
              $this->show();
          }
        }
    }

    public function error_msg($msg){
        $_SESSION['error'] = $msg;
    }

    public function login($str){
        $this->logged = $str;
    }

    public function show_error(){
        return $this->error;
    }

    public function is_logged(){
        if(empty($this->logged)) {
            header('Location: login.php');
        } else {
            $this->welcome($this->logged);
            return $this->logged;
        }
    }

    public function logout(){
       session_start();
       $this->logged = "";
        if(session_destroy()){
            header("Location: index.php");
        }

    }

    public function welcome($user){
        if(!isset($_SESSION['welcome'])) {
            $_SESSION['welcome'] = 1;
            echo "
                <script>
                    alert('Welcome $user!');
                </script>
            ";
        }
    }

    public function alert($msg){
        echo "
                <script>
                    alert('$msg');
                </script>
            ";
    }

    public function success($msg){
        if(isset($_SESSION['success'])) {
            $this->success = $_SESSION['success'];
        } else {
            $_SESSION['success'] = $msg;
        }
    }

    public function show(){
        echo "
                <script>
                    alert('$this->success');
                </script>
            ";
    }

}
?>
