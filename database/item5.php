<!DOCTYPE html>
<html>
<head>
    <title>Database Retrieval</title>
</head>
    <style>
        p {
            font-weight: bold;
        }
    </style>
    <body>

        <?php
            $host = "localhost";
            $conn = "yns";
            $user = "root";
            $pass = "";

            $conn = new mysqli($host,$user,$pass,$conn);
            if ($conn->connect_error) {
                echo $conn->connect_error;
            }
        ?>


        <!--
            1) Retrieve employees whose last name start with "K".
        -->
        <p>1) Retrieve employees whose last name start with "K".</p>
        <?php
            $sql = "SELECT * FROM employees
                    WHERE last_name LIKE 'K%'";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['first_name']." ".$row['last_name']."<br>";
            }
        ?>

        <p>2) Retrieve employees whose last name end with "i".</p>

        <?php
            $sql = "SELECT * FROM employees
                    WHERE last_name LIKE '%i'";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['first_name']." ".$row['last_name']."<br>";
            }
        ?>

        <p>3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date.</p>

        <?php
            $sql = "SELECT first_name,middle_name, last_name,hire_date FROM employees
                    WHERE hire_date BETWEEN '2015-01-01' AND '2015-03-31'
                    ORDER BY hire_date ASC";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['first_name']." ".$row['middle_name']." ".$row['last_name']." - ". date("Y/m/d",strtotime($row['hire_date']))."<br>";
            }
        ?>

        <p>4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.</p>
        <?php
            $sql = "SELECT employees.last_name, temp.last_name as boss FROM employees
	               JOIN (SELECT * FROM employees) temp ON temp.id = employees.boss_id";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['last_name']." - ".$row['boss']."<br>";
            }
        ?>

        <p>5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.</p>
        <?php
            $sql = "SELECT last_name FROM employees
                    JOIN departments ON employees.department_id = departments.id
                    WHERE departments.name ='Sales'";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['last_name']."<br>";
            }
        ?>

        <p>6) Retrieve number of employee who has middle name.</p>
        <?php
            $sql = "SELECT count(*) as count FROM employees WHERE middle_name <> ''";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['count']."<br>";
            }
        ?>

        <p>7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.</p>

        <?php
            $sql = "SELECT departments.name, count(*) as count FROM departments
                     JOIN employees ON departments.id = employees.department_id
                    GROUP BY employees.department_id";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['name']." ".$row['count']."<br>";
            }
        ?>

        <p>8) Retrieve employee's full name and hire date who was hired the most recently.</p>
        <?php
            $sql = "SELECT first_name,middle_name,last_name,hire_date FROM employees
                    ORDER BY hire_date DESC";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['first_name']." ".$row['middle_name']." ".$row['last_name']."-".$row['hire_date']." "."<br>";
            }
        ?>

        <p>9) Retrieve department name which has no employee.</p>
        <?php
            $sql = "SELECT departments.name FROM departments
                    LEFT JOIN employees ON departments.id = employees.department_id
                    GROUP BY employees.department_id
                    HAVING count(employees.department_id) = 0";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['name']."<br>";
            }
        ?>

        <p>10) Retrieve employee's full name who has more than 2 positions.</p>
        <?php
            $sql = "SELECT employees.first_name,employees.last_name FROM employees
                    JOIN employee_positions ON employees.id = employee_positions.employee_id
                    GROUP BY employees.id
				    HAVING count(employees.id) > 1";
            $query = $conn->query($sql);
            while ($row = $query->fetch_assoc()) {
                echo $row['first_name']." ".$row['last_name']."<br>";
            }
        ?>
    </body>
</html>
