<!DOCTYPE html>
<html>
<head>
    <title>Add validation in the user information form(required, numeric, character, mailaddress)</title>
</head>
    <body>
        <!--
            Validate inputted values in PHP programs. Then show error messages if there is any error.
            Try to use regular expression when you validate mail address.
        -->
<?php session_start();?>
        <form method="post" action="item7_res.php">
            <p>
            <label>Enter First Name: <?=(isset($_SESSION['error']['fname']))?$_SESSION['error']['fname']:"";?></label> <br>
            <input type="text" id="fname" name="fname" required>
            </p>
            <p>
            <label>Enter Last Name:<?=(isset($_SESSION['error']['lname']))?$_SESSION['error']['lname']:"";?></label> <br>
            <input type="text" id="lname" name="lname" required>
            </p>
            <p>
            <label>Email Address:<?=(isset($_SESSION['error']['email']))?$_SESSION['error']['email']:"";?></label> <br>
            <input type="text" id="email" name="email" required>
            </p>
            <p>
            <label>Age: <?=(isset($_SESSION['error']['age']))? $_SESSION['error']['age']:"";?></label> <br>
            <input type="text" id="age" name="age" required>
            </p>
            <button type="submit">Submit</button>
        </form>
    </body>
    <?php session_destroy(); ?>
</html>
