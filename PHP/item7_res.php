<?php

/*
 * Displays all information inputed from item7.php
 *
 */
session_start();

function checkRequired($val){
    if(empty($val)){
        return [
            'error' => "ERROR: Field is REQUIRED!"
        ];
    } else {
        return $val;
    }
}

function checkAge($val) {
    if(is_numeric($val)){
        return checkRequired($val);
    } else {
        return [
            'error' => "ERROR: age must be NUMERIC!"
        ];
    }
}

function checkEmail($val){
    if(!filter_var($val,FILTER_VALIDATE_EMAIL)){
        return [
            'error' => "Invalid EMAIL FORMAT!"
        ];
    } else {
        return checkRequired($val);
    }
}

function checkChar($val,$limit) {
    if(!preg_match('/^[a-zA-Z ]*$/',$val)){
        return [
            'error' => "Input must NOT CONTAIN NUMBERS!"
        ];
    } elseif (strlen($val) > $limit) {
        return [
            'error' => "Input must NOT EXCEED $limit characters!"];
    } else {
        return checkRequired($val);
    }
}
    $fname = checkChar("jodan7",50);
    $lname = checkChar($_POST['lname'],50);
    $email = checkEmail($_POST['email']);
    $age = checkAge($_POST['age']);

    $error = 0;
    if (isset($fname['error'])) {
        $error = 1;
        $_SESSION['error']['fname'] = $fname['error'];
    }

    if (isset($lname['error'])) {
        $error = 1;
        $_SESSION['error']['lname'] = $lname['error'];
    }

    if (isset($email['error'])) {
        $error = 1;
        $_SESSION['error']['email'] = $email['error'];
    }

    if (isset($age['error'])) {
        $error = 1;
        $_SESSION['error']['age'] = $age['error'];
    }

    if ($error == 1) {
        header ("Location: item7.php");
    } else {
?>
<p>First Name: <?=$fname;?></p>
<p>Last Name: <?=$lname;?></p>
<p>Email: <?=$email;?></p>
<p>Age: <?=$age;?></p>
<?php
           }
?>
