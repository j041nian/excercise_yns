<!DOCTYPE html>
<html>
<head>
    <title>Store inputted user information into a CSV file</title>
</head>
    <body>
        <!--
            CSV stands for comma separated value. Whenever the user information is inputted, it will be appended at the bottom of the CSV file.
        -->
   <form method="post">
            <p>
            <label>Enter First Name:</label>
            <input type="text" id="fname" name="fname" >
            </p>
            <p>
            <label>Enter Last Name:</label>
            <input type="text" id="lname" name="lname" >
            </p>
            <p>
            <label>Email Address</label>
            <input type="text" id="email" name="email" >
            </p>
            <p>
            <label>Age:</label>
            <input type="text" id="age" name="age">
            </p>
            <input type="submit" name="submit" value="Submit">
        </form>


        <?PHP
         $file = 'item8.csv';
         $path = realpath($file);
         if ($path === false && !file_exists($path)) {
             fopen($file, "w");
         }

         if (isset($_POST['submit'])){
             $content = $_POST['fname'].",".$_POST['lname'].",".$_POST['email'].",".$_POST['age'];
            file_put_contents($file,$content, FILE_APPEND);
         }
        ?>
    </body>
</html>
