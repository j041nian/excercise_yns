<!DOCTYPE html>
<html>
<head>
    <title>Solve FizzBuzz problem</title>
</head>
    <body>
        <!--
            Display 1 text box in a page. If you enter number, which means last number, in the text box and press the submit button, it will calculate and show the result.
        -->

        <form method="post">
            <label for="val1">Enter number:
            </label>
            <input type="number" name="val1" id="val1">

            <input type="submit" name="submit" value="Submit">
        </form>

        <?PHP
         if (isset($_POST['submit'])) {
            $val1 = $_POST['val1'];

             for ($i = 1; $i <= $val1; $i++){

                 if ($i%3 == 0) {
                     if ($i%5 == 0) {
                         echo "FizzBuzz ";
                     }
                 } elseif ($i%5 == 0) {
                         echo "Fizz ";
                } else {
                    echo $i." ";
                 }
             }
         }
        ?>
    </body>
</html>
