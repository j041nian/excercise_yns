<!DOCTYPE html>
<html>
<head>
    <title>Upload images</title>
</head>
    <body>
        <!--
            Add upload image feature for uploading user profile image.
        -->

        <?PHP
            $file = 'item9.csv';
            $csv = explode("\n",file_get_contents($file));
        ?>

        <form method="post" enctype="multipart/form-data">
       <table border='1' cellpadding="10">
            <thead>
                <th>#</th>
                <th>Profile Pic</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Age</th>
            </thead>
        <?PHP

            $file = 'item9.csv';
            $csv = explode("\n",file_get_contents($file));
           $page = 0;

          //starting count number for pagination
           $count = 0;

           //end count number;
           $end = 10;

           if (isset($_GET['page'])){
               $page = $_GET['page'];
               $count = ($page*10) - 9;
               $end = $page*10;
           }

           $num = 0+$count;
           for($i=$count; $i<=count($csv)-1; $i++){
               if ($num <= $end) {
                    echo "<tr>";
                echo "<td>".$num++."</td>";
                $data = explode(",",$csv[$i]);

                $image = "images/".$data[2]."/picture.jpg";

                if(file_exists($image)) {
                    echo "<td><img src='".$image."' width=100></td>";
                } else {
                    echo "<td><a href='item10_upload.php?user=".$data['2']."'>Upload</a></td>";
                }

                    echo "<td>".$data[0]."</td>";
                    echo "<td>".$data[1]."</td>";
                    echo "<td>".$data[2]."</td>";
                    echo "<td>".$data[3]."</td>";

                echo "</tr>";
               }
           }

        pagination(count($csv));


           function pagination($x){
               $num = round($x/10);
               $count = $num;

               if ($num <= $x){
                   $count = $num+1;
               }

               echo "Page: ";
               for($i=1;$i<=$count;$i++){
                   echo "<a href='?page=$i'>".$i."</a> &nbsp;".PHP_EOL;
               }


           }

        ?>
        </table>
        </form>

        <?PHP
            if (isset($_POST['submit'])){

            $base_dir = 'images/';
            $file = $_FILES['picture']['name'];

                if (file_exists($base_dir.$file)){
                   echo "ERROR: FILE ALREADY EXIST!";
                } else {
                    if (move_uploaded_file($_FILES['picture']['tmp_name'],$base_dir.$file)){
                        echo "SUCCESS: File has been uploaded!";
                    }
                }
            }
        ?>
    </body>
</html>
