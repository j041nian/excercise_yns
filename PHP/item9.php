<!DOCTYPE html>
<html>
<head>
    <title>Show the user information using table tags.</title>
</head>
    <body>
        <!--
            Create new page for listing up all the user information using table tags.
        -->
        <table border='1' cellpadding="10">
            <thead>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Age</th>
            </thead>
        <?PHP

            $file = 'item9.csv';
            $csv = explode("\n",file_get_contents($file));
            foreach ($csv as $item) {
                echo "<tr>";
                $data = explode(",",$item);
                foreach ($data as $value) {
                    echo "<td>".$value."</td>";
                }
                echo "</tr>";
            }
        ?>
        </table>
    </body>
</html>
