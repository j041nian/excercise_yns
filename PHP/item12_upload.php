<!DOCTYPE html>
<html>
<head>
    <title>Upload images</title>
</head>
    <body>
        <!--
            Add upload image feature for uploading user profile image.
        -->

        <?php
            $file = 'item9.csv';
            $csv = explode("\n",file_get_contents($file));
        ?>
        <h1>JPeg Images only</h1>
        <form method="post" enctype="multipart/form-data">

             <input type='file' name='picture'>
             <p><input type="submit" name="submit" value="Submit"></p>
        </form>

        <?php
            if (isset($_POST['submit'])){

            $base_dir = 'images/';
            $path = checkDirectory($base_dir);

            $file = $_FILES['picture']['name'];
            $name = "picture.".explode(".",$file)[1];
            echo $name;
            $folder = checkDirectory($base_dir.$_GET['user']);

            if (exif_imagetype($_FILES['picture']['tmp_name']) != IMAGETYPE_JPEG){
                    echo 'Not a JPEG image';
                } else {
                    // create file on user folder
                    if (move_uploaded_file($_FILES['picture']['tmp_name'],$folder."/".$name)){
                        header("Location: item10.php");
                    }
                }
            }

        function checkDirectory($dir) {

            // creates the image directory if not exists
            $path = realpath($dir);
            if ($path === false) {
                mkdir($dir, 0777);
            } else {
                if (!is_dir($path)) {
                   mkdir($dir, 0777);
                }
            }
            return $dir;
        }


        ?>
    </body>
</html>
