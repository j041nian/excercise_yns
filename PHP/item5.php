<!DOCTYPE html>
<html>
<head>
    <title>Input date. Then show 3 days from inputted date and its day of the week</title>
</head>
    <body>
        <!--
            Display 1 text box in a page. If you enter date in the text box and press the submit button, it will calculate and show the result.
        -->
        <form method="post">
            <label for="dateval">Enter Date:</label>
            <input type="text" id="dateval" name="dateval" placeholder="YYYY-MM-DD" autocomplete="off" required>

            <input type="submit" name="Submit" value="Submit">
        </form>

        <?PHP
            if (isset($_POST['Submit'])){

                $date = date_create($_POST['dateval']);
                $checker = date_format($date,"Y-m-d");
                if ($date && $checker == $_POST['dateval'] ){
                   echo dateResult($_POST['dateval']);
                } else {
                    echo "<pre>Invalid Date</pre>";
                }
            }

        function dateResult($x) {
            $date = date("F d, Y l",strtotime($x." +3 days"));
            return  $date;
        }
        ?>
    </body>
</html>
