<!DOCTYPE html>
<html>
<head>
    <title>Upload images</title>
</head>
    <body>
        <!--
            Add upload image feature for uploading user profile image.
        -->

        <?PHP
        require('session.php');
            $file = 'list.csv';
            $csv = explode("\n",file_get_contents($file));
        ?>
        <br>
        <a href="home.php"> &#60;&#60;&#60; Back</a>
        <form method="post" enctype="multipart/form-data">
       <table border='1' cellpadding="10">
            <thead>
                <th>#</th>
                <th>Profile Pic</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Age</th>
            </thead>
        <?PHP

            $file = 'list.csv';
            $csv = explode("\n",file_get_contents($file));
           $page = 0;

          //starting count number for pagination
           $count = 0;

           //end count number;
           $end = 10;

           if(isset($_GET['page'])){
               $page = $_GET['page'];
               $count = ($page*10) - 9;
               $end = $page*10;
           }


           for($i =$count; $i<=count($csv)-1; $i++){
               $num = $i+1;
               if($num <= $end) {
                    echo "<tr>";
                echo "<td>".$num."</td>";
                $data = explode(",",$csv[$i]);
                if(file_exists('images/picture'.$i.'.jpg')) {
                    echo "<td><img src='images/picture".$i.".jpg' height='50'></td>";
                } else {
                    echo "<td><a href='upload.php?id=".$i."'>Upload</a></td>";
                }
                foreach($data as $value) {
                    echo "<td>".$value."</td>";
                }
                echo "</tr>";
               }
           }

        ?>
        </table>
        </form>

<?PHP

//pagination function
function pagination($x){
   $num = round($x/10);
   $count = $num;

   if($num <= $x){
       $count = $num+1;
   }

   echo "Page: ";
   for($i=1;$i<=$count;$i++){
       echo "<a href='?page=$i'>".$i."</a> &nbsp;".PHP_EOL;
   }


}

        //generate pagination links
        pagination(count($csv));


        // Submit process
            if(isset($_POST['submit'])){
            $base_dir = 'images/';
            $file = $_FILES['picture']['name'];
                    if(move_uploaded_file($_FILES['picture']['tmp_name'],$base_dir.$file)){
                        echo "SUCCESS: File has been uploaded!";
                    }
                }
        ?>
    </body>
</html>
