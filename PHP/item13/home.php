<!DOCTYPE html>
<html>
<head>
    <title>Application</title>
</head>
    <body>
<?PHP
    require('session.php');
?>
        <h3>Welcome <?=$_SESSION['logged'];?></h3>
   <form method="post">
            <p>
            <label>Enter First Name:</label>
            <input type="text" id="fname" name="fname" >
            </p>
            <p>
            <label>Enter Last Name:</label>
            <input type="text" id="lname" name="lname" >
            </p>
            <p>
            <label>Email Address</label>
            <input type="text" id="email" name="email" >
            </p>
            <p>
            <label>Age:</label>
            <input type="text" id="age" name="age">
            </p>
            <input type="submit" name="submit" value="Submit">
        </form>


        <?PHP

        function checkRequired($x){
            if(empty($x)){
                return "ERROR: Field is REQUIRED!";
            } else {
                return $x;
            }
        }

        function checkAge($x) {
            if(is_numeric($x)){
                return checkRequired($x);
            } else {
                return "<b>ERROR: age must be NUMERIC!</b>";
            }
        }

        function checkEmail($x){
            if(!filter_var($x,FILTER_VALIDATE_EMAIL)){
                return "<b>ERROR: Invalid EMAIL FORMAT!</b>";
            } else {
                return checkRequired($x);
            }
        }

        function checkChar($x) {
            if(preg_match('#[0-9]#',$x)){
                return "<b>ERROR: Name must NOT CONTAIN NUMBERS!</b>";
            } else {
                return checkRequired($x);
            }
        }

         $file = 'list.csv';
         if(isset($_POST['submit'])){
             $error = 0;
             $msg = array();
             if(checkChar($_POST['fname']) !== $_POST['fname']){
                 $msg[] = checkChar($_POST['fname']);
                 $error = 1;
             }

             if(checkChar($_POST['lname']) !== $_POST['lname']){
                 $error = 1;
             }

             if(checkEmail($_POST['email']) !== $_POST['email']){
                 $msg[] = checkEmail($_POST['email']);
                 $error = 1;
             }

             if(checkAge($_POST['age']) !== $_POST['age']){
                 $msg[] = checkAge($_POST['age']);
                 $error = 1;
             }

             if($error == 0) {
                 $content =PHP_EOL.$_POST['fname'].",".$_POST['lname'].",".$_POST['email'].",".$_POST['age'];
                $save = file_put_contents($file,$content, FILE_APPEND);
                 if($save){
                     echo "<p>Information saved!</p>";
                 }
             } else {
                echo implode("<br>",$msg);
             }
         }
        ?>
        <p>
            <a href="list.php">Go to User List..</a>
        </p>
    </body>
</html>
