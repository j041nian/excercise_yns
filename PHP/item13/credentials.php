<?PHP
session_start();

if(isset($_POST['user']) && isset($_POST['pass'])){
    $user = $_POST['user'];
    $pass = $_POST['pass'];

    if($user === 'admin' && $pass === 'admin') {
        $_SESSION['logged'] = $user;
        header("Location: home.php");
    } else {
        header("Location: login.php?error=2");
    }
} else {
    header("Location: login.php?error=1");
}
?>
