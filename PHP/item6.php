<!DOCTYPE html>
<html>
<head>
    <title>Input user information. Then show it in next page</title>
</head>
    <body>
        <!--
            Display html components in a page. If you enter values in each component and press the submit button, it will show the inputted data in another page.
        -->

        <form method="post" action="item6_res.php">
            <p>
            <label>Enter First Name:</label>
            <input type="text" id="fname" name="fname" required>
            </p>
            <p>
            <label>Enter Middle Name:</label>
            <input type="text" id="mname" name="mname" required>
            </p>
            <p>
            <label>Enter Last Name:</label>
            <input type="text" id="lname" name="lname" required>
            </p>
            <p>
            <label>Enter Address</label>
            <input type="text" id="address" name="address" required>
            </p>
            <button type="submit">Submit</button>
        </form>

    </body>
</html>


