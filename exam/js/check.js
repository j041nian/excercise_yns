function checkResult(arr) {
    $.each(arr,function(indexKey, value){
//        elem = $("input[name='"+indexKey+"']:checked");
        elem = $("form").find("input[name='"+indexKey+"']:checked");
        $("button").hide();
        listElem = $("form").find("li:contains('"+value+"')");

        $("input").prop("disabled",true);
       if(elem.val() == value) {
           elem.parent().append(" <span class='fa fa-check text-success'></span>");
       } else {
           elem.parent().append(" <span class='fa fa-times text-danger'></span>");
           listElem.addClass("list-group-item-warning");
       }

      if(elem.length == 0) {
          listElem.addClass("list-group-item-warning");
      }
    });
}
