<?PHP
namespace Database;
class Database {

    private $Db;
    private static $instance = NULL;

    public function __construct(){
        try {
             $this->Db = new \PDO("mysql:host=127.0.0.1;dbname=exam","root","");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function query($sql,$param = array()) {
        try {
         $query = $this->Db->prepare($sql);
         $result = $query->execute($param);
        } catch (PDOException $e) {
            return $e->getMessage();
        }
//        var_dump($query->debugDumpParams());   //PDO debug sql statement
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function select($col,$table,$param = NULL,$values = array()) {
        $sql = "SELECT $col FROM $table";

        if(!is_null($param)) {
            $sql .= " WHERE $param";
        }
        return $this->query($sql,$values);
    }

    public function getInstance(){
        if(self::$instance === NULL) {
            self::$instance = new Database();
            return self::$instance;
        } else {
            return self::$instance;
        }
    }
}
?>
