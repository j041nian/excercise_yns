<?PHP

namespace Question;
require_once('dbconfig.php');
use Database\Database;
class Question {

    public function prepare($arr){
        shuffle($arr);
        echo "<form>";
        echo "<table class='table table-stripe table-bordered'>";
        $i = 1;
        foreach ($arr as $item) {
            echo "<tr class='question'>";
            echo "<td>".$i++.") ".$item['question']."</td>";
            echo "</tr><tr>";
            echo "<td>".$this->choices($item['id'])."</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo "<button class='btn btn-primary'>Submit</button>";
        echo "</form>";
    }

    private function choices($id) {
        $db = Database::getInstance();
        $result = $db->select("*","tbl_choices","question_id = :id",[':id'=>$id]);
        $list = "<ul class='list-group'>";
        shuffle($result);
        foreach ($result as $item) {
            $list .= "<li class='list-group-item'>
                        &nbsp;  <input type='radio' class='form-check-input' value='".$item['content']."' name='q".$id."'>"
                        .$item['content']."</li>";
        }
        $list .= "</ul>";
        return $list;
    }

    public function answer($arr){
        $db = Database::getInstance();
        $result = $db->select("tbl_questions.id,tbl_choices.content","tbl_questions INNER JOIN tbl_choices ON tbl_questions.answer = tbl_choices.id");
        $score = 0;
        $res_arr = [];

        //place correct answers in an array
        foreach ($result as $item){
            ${'q'.$item['id']} = $item['content'];
            $res_arr['q'.$item['id']] = $item['content'];
        }

        foreach (array_keys($arr) as $k){
            if ($arr[$k] == ${$k}) {
                $score++;
            }
        }
        $res_arr['score'] = $score;
        return json_encode($res_arr);
    }
}
?>
