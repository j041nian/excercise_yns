<?PHP

require('dbconfig.php');
require('question.php');

use Database\Database;
use Question\Question;

$db = Database::getInstance();
$q = new Question;

$list = $db->select("*","tbl_questions");
?>

<style>
    .box {
        padding-top: 50px;
        text-align: left;
    }

    .table {
        background: #FFF;
    }
</style>

<div class="box">
    <label class="score"></label>
        <?PHP $q->prepare($list);?>
</div>


<script>
    $(document).ready(function(){
        $("button").click(function(e){
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "check.php",
                data: $("form").serialize(),
                success: function(result){
                   arr_res = $.parseJSON(result);
                   checkResult(arr_res);
                   alert("You got "+arr_res.score+" out of 10!");
                   $(".score").html("Score: " +arr_res.score+ "/ 10");
                }
            });
        });
    });
</script>
