-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2018 at 10:09 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_choices`
--

CREATE TABLE `tbl_choices` (
  `id` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_choices`
--

INSERT INTO `tbl_choices` (`id`, `content`, `question_id`) VALUES
(1, 'Amazon River', 1),
(2, 'Nile River', 1),
(3, 'Pasig River', 1),
(4, 'Keanu Reeves', 2),
(5, 'Hugh Jackman', 2),
(6, 'Coco Martin', 2),
(7, 'Madrid', 3),
(8, 'Makati', 3),
(9, 'Ulaanbaatar', 3),
(10, 'Harry Potter', 8),
(11, 'Titanic', 8),
(12, 'Probinsyano', 8),
(13, 'Australia', 4),
(14, 'Greenland', 4),
(15, 'Boracay', 4),
(16, 'Manila Bay', 5),
(17, 'China', 5),
(18, 'Moon', 5),
(19, 'Pluto', 6),
(20, 'Mars', 6),
(21, 'Uranus', 6),
(22, 'Peter Jackson', 7),
(23, 'Michael Bay', 7),
(24, 'George Lucas', 7),
(25, 'Aquaphobia', 9),
(26, 'Arachnaphobia', 9),
(27, 'Aerophobia', 9),
(28, 'Ron Weasley', 10),
(29, 'Rubeus Hagrid', 10),
(30, 'Severus Snape', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_examinee`
--

CREATE TABLE `tbl_examinee` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `q_series` varchar(50) NOT NULL DEFAULT '0',
  `a_series` varchar(50) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `answer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `question`, `answer`) VALUES
(1, 'What is the world\'s longest river?', 1),
(2, 'Who played Neo in the movie: The Matrix?', 4),
(3, 'What is the capital city of Spain?', 7),
(4, 'Name the world\'s biggest island.', 14),
(5, 'Where would you find the Sea of Tranquility?', 18),
(6, 'Name the seventh planet from the sun.', 21),
(7, 'Name the director of the Lord of the Rings trilogy.', 22),
(8, 'My Heart Will Go On came from the movie?', 11),
(9, 'The fear of drowning.', 25),
(10, 'Who is the half blood prince in the movie: Harry Harry Potter', 30);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_choices`
--
ALTER TABLE `tbl_choices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question` (`question_id`);

--
-- Indexes for table `tbl_examinee`
--
ALTER TABLE `tbl_examinee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answer` (`answer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_choices`
--
ALTER TABLE `tbl_choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_examinee`
--
ALTER TABLE `tbl_examinee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_choices`
--
ALTER TABLE `tbl_choices`
  ADD CONSTRAINT `question` FOREIGN KEY (`question_id`) REFERENCES `tbl_questions` (`id`);

--
-- Constraints for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD CONSTRAINT `answer` FOREIGN KEY (`answer`) REFERENCES `tbl_choices` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
